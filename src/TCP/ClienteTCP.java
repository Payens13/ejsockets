package TCP;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class ClienteTCP {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Creando y conectando el socket stream cliente");
        try {
            Socket cliente = new Socket("localhost", 50005);

            // Configura los streams de entrada y salida
            DataInputStream input = new DataInputStream(cliente.getInputStream());
            DataOutputStream output = new DataOutputStream(cliente.getOutputStream());

            while (true) {
                System.out.println("Ingrese la operación (+, -, *, /) o \"salir\":");
                String operacion = sc.nextLine();
                if (operacion.equalsIgnoreCase("salir")) {
                    break;
                }
                output.writeUTF(operacion);

                System.out.println("Ingrese el primer número:");
                double num1 = sc.nextDouble();
                output.writeDouble(num1);

                System.out.println("Ingrese el segundo número:");
                double num2 = sc.nextDouble();
                output.writeDouble(num2);

                // Recibe el resultado del servidor
                double resultado = input.readDouble();
                System.out.println("Resultado recibido del servidor: " + resultado);
            }

            System.out.println("Cerrando conexión con el cliente...");
            input.close();
            output.close();
            cliente.close();
            System.out.println("Terminando");

        } catch (IOException e) {
            System.err.println("Error al crear el socket.");
            System.out.println(e.getMessage());
        }
    }
}
