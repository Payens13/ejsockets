package TCP;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Semaphore;

public class ServidorTCP {
    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(50005);
            System.out.println("Servidor de calculadora iniciado. Esperando conexiones...");

            // Crear un semáforo para controlar el número máximo de clientes
            int maxClientes = 5;
            Semaphore semaphore = new Semaphore(maxClientes);

            while (true) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("Cliente conectado: " + clientSocket.getInetAddress());

                // Crear un nuevo hilo para manejar la comunicación con el cliente
                Thread clientThread = new Thread(new ClientHandler(clientSocket, semaphore));
                clientThread.start();
            }
        } catch (IOException e) {
            System.err.println("Error al crear el socket del servidor.");
            System.out.println(e.getMessage());
        }
    }
}

class ClientHandler implements Runnable {
    private Socket clientSocket;
    private Semaphore semaphore;

    public ClientHandler(Socket socket, Semaphore semaphore) {
        this.clientSocket = socket;
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        try {
            // Adquirir un permiso del semáforo
            semaphore.acquire();

            // Configurar los streams de entrada y salida
            DataInputStream input = new DataInputStream(clientSocket.getInputStream());
            DataOutputStream output = new DataOutputStream(clientSocket.getOutputStream());

            // Recibir la operación y los operandos del cliente
            String operacion = input.readUTF();
            double num1 = input.readDouble();
            double num2 = input.readDouble();

            // Realizar la operación
            double resultado = 0.0;
            switch (operacion) {
                case "+":
                    resultado = num1 + num2;
                    break;
                case "-":
                    resultado = num1 - num2;
                    break;
                case "*":
                    resultado = num1 * num2;
                    break;
                case "/":
                    resultado = num1 / num2;
                    break;
                default:
                    output.writeUTF("Operación no válida");
                    break;
            }

            // Enviar el resultado al cliente
            output.writeDouble(resultado);

            // Cerrar los streams y el socket
            input.close();
            output.close();
            clientSocket.close();

            // Liberar el permiso del semáforo
            semaphore.release();
        } catch (IOException | InterruptedException e) {
            System.err.println("Error al manejar la conexión con el cliente.");
            System.out.println(e.getMessage());
        }
    }
}
