import java.io.IOException;
import java.net.*;
import java.util.concurrent.Semaphore;

public class ServidorUDP {
    public static void main(String[] args) {
        try {
            InetSocketAddress addr = new InetSocketAddress("127.0.0.1", 50005);
            DatagramSocket socket = new DatagramSocket(addr);
            System.out.println("Servidor conectado. Esperando conexiones...");

            // Crear un semáforo para controlar el número máximo de clientes
            Semaphore semaphore = new Semaphore(5); // Máximo 5 clientes

            while (true) {
                DatagramPacket paquete = new DatagramPacket(new byte[1024], 1024);
                socket.receive(paquete);

                // Verificar si hay permisos disponibles en el semáforo
                if (semaphore.tryAcquire()) {
                    // Crear un nuevo hilo para manejar la comunicación con el cliente
                    Thread clientThread = new Thread(new ClientHandler(socket, paquete, semaphore));
                    clientThread.start();
                } else {
                    System.out.println("Número máximo de clientes alcanzado. Rechazando conexión.");
                    // Puede enviar un mensaje al cliente indicando que no se puede aceptar más conexiones.
                }
            }
        } catch (IOException e) {
            System.out.println("Error al crear el socket del servidor: " + e.getMessage());
        }
    }
}

class ClientHandler implements Runnable {
    private DatagramSocket socket;
    private DatagramPacket paquete;
    private Semaphore semaphore;

    public ClientHandler(DatagramSocket socket, DatagramPacket paquete, Semaphore semaphore) {
        this.socket = socket;
        this.paquete = paquete;
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        try {
            String mensaje = new String(paquete.getData()).trim();
            System.out.println("Recibiendo mensaje del cliente: " + mensaje);

            // Parsear la operación y los operandos
            String[] partes = mensaje.split(",");
            String operacion = partes[0];
            double num1 = Double.parseDouble(partes[1]);
            double num2 = Double.parseDouble(partes[2]);

            // Realizar la operación (implementa el resto de la lógica aquí)
            double resultado = 0.0;
            switch (operacion) {
                case "+":
                    resultado = num1 + num2;
                    break;
                case "-":
                    resultado = num1 - num2;
                    break;
                case "*":
                    resultado = num1 * num2;
                    break;
                case "/":
                    resultado = num1 / num2;
                    break;
                default:
                    System.out.println("Operación no válida");
                    break;
            }

            // Enviar el resultado al cliente
            byte[] respuesta = String.valueOf(resultado).getBytes();
            DatagramPacket respuestaPaquete = new DatagramPacket(respuesta, respuesta.length,
                    paquete.getAddress(), paquete.getPort());
            socket.send(respuestaPaquete);

            // Liberar el semáforo después de procesar la solicitud
            semaphore.release();
        } catch (IOException e) {
            System.out.println("Error al manejar la conexión con el cliente: " + e.getMessage());
        }
    }
}
