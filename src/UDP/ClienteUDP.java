package UDP;

import java.io.IOException;
import java.net.*;
import java.util.Scanner;

public class ClienteUDP {
    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);

            // Solicitar la operación y los operandos al usuario
            System.out.print("Introduce la operación (por ejemplo, +, -, *, /): ");
            String operacion = scanner.nextLine();

            System.out.print("Introduce el primer operando: ");
            double num1 = scanner.nextDouble();

            System.out.print("Introduce el segundo operando: ");
            double num2 = scanner.nextDouble();

            // Crear el mensaje con la operación y los operandos
            String mensaje = operacion + "," + num1 + "," + num2;

            DatagramSocket datagramSocket = new DatagramSocket();
            InetAddress serverAddr = InetAddress.getByName("localhost");

            // Construir el paquete
            DatagramPacket paquete = new DatagramPacket(mensaje.getBytes(), mensaje.length(), serverAddr, 50005);
            datagramSocket.send(paquete);
            System.out.println("Mensaje enviado al servidor");

            // Esperar la respuesta del servidor
            byte[] respuesta = new byte[1024];
            paquete = new DatagramPacket(respuesta, respuesta.length);
            datagramSocket.receive(paquete);
            System.out.println("Resultado recibido del servidor: " + new String(respuesta));

            datagramSocket.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
